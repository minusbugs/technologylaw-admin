<?php
	$this->load->view('includes/header');
?>
<section>
	<div class="mainpanel">

      <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
          <li><a href="<?=site_url()?>/adminlogin/dashboard"><i class="fa fa-home mr5"></i> Home</a></li>
          <li><a href="<?=site_url()?>/adminlogin/advertisement">Advertisement</a></li>
          <li class="active">Add Advertisement</li>
        </ol>

       <div class="col-md-12">
          <div class="panel">
              <div class="panel-heading nopaddingbottom " >
                	<h1 class="panel-title" align="center">Add Advertisement</h1>
	              <?php
	              	if($status!=NULL){
	              ?>
					<div class="alert alert-info alert-margin">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<strong><?=$status?></strong>
					</div>
	              <?php
	              	}	
	              ?>
              </div>
             	
              <div class="panel-body">
                <hr>
                <div class="form-horizontal">
                <?=form_open_multipart('adminlogin/insertAdvertisement');?>
				<div class="form-group">
					<label class="col-sm-3 control-label">Advertisement URL <span class="text-danger">*</span></label>
					<div class="col-sm-8">
						<input type="text" name="url" class="form-control" required />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Advertisement Image <span class="text-danger"></span></label>
					<div class="col-sm-8">
						<input type="file" name="adsimage" class="form-control" required />
					</div>
				</div>
                  <hr>

                  <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                      <input type="submit" class="btn btn-success btn-quirk btn-wide mr5" value="Add Advertisement"></button>
                    </div>
                  </div>
                  <?=form_close()?>
                </div> 
              </div><!-- panel-body -->
          </div><!-- panel -->

        </div>

      </div><!-- contentpanel -->
    </div><!-- mainpanel -->		
</section>
<?php
	$this->load->view('includes/footer');
?>