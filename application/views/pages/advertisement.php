<?php
	$this->load->view('includes/header');
?>
<section>
	  <div class="mainpanel">

      <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
          <li><a href="<?=site_url()?>/adminlogin/dashboard"><i class="fa fa-home mr5"></i> Home</a></li>
          <li class="active">Advertisements</a></li>
        </ol>
        <div class="header-alignment">
           <h1 class="panel-title" align="center">Advertisementss</h1>
        </div>
      <div class="row">
        <div class="col-sm-8 col-md-12 col-lg-12">
          <div class="row filemanager">
          	<?php
          		if($advertisement!=null){
          			foreach ($advertisement as $ads) {
          	?>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 audio">
              <div class="thmb">
                <div class="btn-group fm-group">
                  <button type="button" class="btn btn-default dropdown-toggle fm-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu pull-right fm-menu" role="menu">
                    <li data-toggle="modal" data-target="#editModal<?=$ads->	AdvertisementId?>"><a href="#"><i class="fa fa-pencil"></i> Edit</a></li>
                    <li data-toggle="modal" data-target="#deleteModal<?=$ads->	AdvertisementId?>"><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                  </ul>
                </div><!-- btn-group -->
                <div class="thmb-prev thmb-width">
                  <img src="<?=base_url()?>assets/advertisement/<?=$ads->AdvertisementImage?>" class="img-responsive" alt="" />
                </div>	
              </div><!-- thmb -->
            </div><!-- col-xs-6 -->

            <div class="modal bounceIn animated" id="editModal<?=$ads->	AdvertisementId?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" align="center">Edit	Advertisement
              </div>
              <div class="modal-body">
            <div id="basicForm"  class="form-horizontal">
            <?=form_open_multipart('adminlogin/editAdvertisement');?>
              
              <div class="form-group">
              	<div class="col-md-3"></div>
              	<div class="thmb-prev col-md-6">
                  <img  class="thmb-height" src="<?=base_url()?>assets/advertisement/<?=$ads->AdvertisementImage?>" class="img-responsive" alt="" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Article Image <span class="text-danger">*</span></label>
                <div class="col-sm-8">
                  <input type="file" name="advertisementimage" class="form-control" "required />
                </div>
              </div>
              <div class="form-group">
                <input type="hidden" name="advertisementid" value="<?=$ads->AdvertisementId?>">
                <label class="col-sm-3 control-label">Advertisement URL<span class="text-danger">*</span></label>
                <div class="col-sm-8">
                  <input type="text" name="advertisementurl" class="form-control" value="<?=$ads->AdvertisementUrl?>"required />
                </div>
              </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Save changes" />


                <?=form_close()?>
                </div>
              </div>
            </div><!-- modal-content -->
          </div><!-- modal-dialog -->
        </div><!-- modal -->

        <div class="modal bounceIn animated" id="deleteModal<?=$ads->AdvertisementId?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              </div>
              <div class="modal-body ">
                 <h4 class="text-color">Do you Want Delete this...</h4>
              </div>
              <?=form_open('Adminlogin/deleteAdvertisement')?>
              <input type="hidden" name="advertisementid" value="<?=$ads->AdvertisementId?>">
              <input type="hidden" name="advertisementimage" value="<?=$ads->AdvertisementImage?>">
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Delete" />
              </div>
              <?=form_close()?>
            </div><!-- modal-content -->
          </div><!-- modal-dialog -->
        </div>
           <?php
           		}
           	}
           ?>
          </div><!-- row -->
        </div><!-- col-sm-9 -->
      </div>
    </div>

  </div><!-- mainpanel -->
  <ul class="pagination pull-right">
        <?php foreach ($links as $link) 
        {
          ?><li><?php echo $link?></li>
          <?php }
        ?>
   </ul>
</section>
<?php
	$this->load->view('includes/footer');
?>
<script>
  jQuery(document).ready(function(){

    'use strict';

    jQuery('.thmb').hover(function(){
      var t = jQuery(this);
      t.find('.ckbox').show();
      t.find('.fm-group').show();
    }, function() {
      var t = jQuery(this);
      if(!t.closest('.thmb').hasClass('checked')) {
        t.find('.ckbox').hide();
        t.find('.fm-group').hide();
      }
    });

    jQuery('.ckbox').each(function(){
      var t = jQuery(this);
      var parent = t.parent();
      if(t.find('input').is(':checked')) {
        t.show();
        parent.find('.fm-group').show();
        parent.addClass('checked');
      }
    });


    jQuery('.ckbox').click(function(){
      var t = jQuery(this);
      if(!t.find('input').is(':checked')) {
        t.closest('.thmb').removeClass('checked');
        enable_itemopt(false);
      } else {
        t.closest('.thmb').addClass('checked');
        enable_itemopt(true);
      }
    });

    jQuery('#selectall').click(function(){
      if(jQuery(this).is(':checked')) {
        jQuery('.thmb').each(function(){
          jQuery(this).find('input').attr('checked',true);
          jQuery(this).addClass('checked');
          jQuery(this).find('.ckbox, .fm-group').show();
        });
        enable_itemopt(true);
      } else {
        jQuery('.thmb').each(function(){
          jQuery(this).find('input').attr('checked',false);
          jQuery(this).removeClass('checked');
          jQuery(this).find('.ckbox, .fm-group').hide();
        });
        enable_itemopt(false);
      }
    });

    function enable_itemopt(enable) {
      if(enable) {
        jQuery('.itemopt').removeClass('disabled');
      } else {

        // check all thumbs if no remaining checks
        // before we can disabled the options
        var ch = false;
        jQuery('.thmb').each(function(){
          if(jQuery(this).hasClass('checked'))
            ch = true;
        });

        if(!ch)
          jQuery('.itemopt').addClass('disabled');
      }
    }

    jQuery("a[data-rel^='prettyPhoto']").prettyPhoto();

  });

</script>