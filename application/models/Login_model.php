<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model{
	 function __construct()
    {
       $this->load->database();
    }
	public function login($emailId,$password){
		$this->db->select('*');
		$this->db->from('login_tbl');
		$this->db->where(array('EmailId'=>$emailId,'UserPassword'=>$password));
		$query=$this->db->get();
		return $query->row();
	}
	public function changePassword($data,$emailId){
		$this->db->where('EmailId',$emailId);
		return $this->db->update('login_tbl',$data);
	}
}