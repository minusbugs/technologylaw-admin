<?php
	$this->load->view('includes/header');
?>
<section>
	<div class="mainpanel">

      <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
          <li><a href="<?=site_url()?>/adminlogin/dashboard"><i class="fa fa-home mr5"></i> Home</a></li>
          <li><a href="<?=site_url()?>/adminlogin/articles">Articles</a></li>
          <li class="active">Add Articles</li>
        </ol>

       <div class="col-md-12">
          <div class="panel">
              <div class="panel-heading nopaddingbottom " >
                	<h1 class="panel-title" align="center">Add Articles</h1>
	              <?php
	              	if($status!=NULL){
	              ?>
					<div class="alert alert-info alert-margin">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<strong><?=$status?></strong>
					</div>
	              <?php
	              	}	
	              ?>
              </div>
             	
              <div class="panel-body">
                <hr>
                <div class="form-horizontal">
                <?=form_open_multipart('adminlogin/insertArticle');?>
				<div class="form-group">
					<input type="hidden" name="articleid">
					<label class="col-sm-3 control-label">Article Name <span class="text-danger">*</span></label>
					<div class="col-sm-8">
						<input type="text" name="articlename" class="form-control" required />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Article Description <span class="text-danger">*</span></label>
					<div class="col-sm-8">
						<textarea id="wysiwyg" name="articledescription"placeholder="Enter text here..." class="form-control" rows="10"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">article Image <span class="text-danger"></span></label>
					<div class="col-sm-8">
						<input type="file" name="articleimage" class="form-control" />
					</div>
				</div>
                  <hr>

                  <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                      <input type="submit" class="btn btn-success btn-quirk btn-wide mr5 " value="Add Article"></button>
                    </div>
                  </div>
                  <?=form_close()?>
                </div>
              </div><!-- panel-body -->
          </div><!-- panel -->

        </div>

      </div><!-- contentpanel -->
    </div><!-- mainpanel -->		
</section>	
<?php
	$this->load->view('includes/footer');
?>

<script>
$(document).ready(function(){

  'use strict';

  // HTML5 WYSIWYG Editor
  $('#wysiwyg').wysihtml5({
  	
    toolbar: {
      fa: false,
      "image": false,
    }
  });
});
</script>