<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class articles_model extends CI_Model{
	 function __construct()
    {
       $this->load->database();
    }
    public function articleCount(){
        return $this->db->count_all("article_tbl");
    }
    public function addData($data){
    	$returnId=0;
        $returnId=$this->db->insert('article_tbl',$data);
        return $returnId;
    }
    public function getDataOncreate($limit, $id){
        if($id>1)
        {
            $offset = ($id-1)*$limit;
            $this->db->limit($limit,$offset);
        }else
        {
            $this->db->limit($limit,$id);
        }
        $this->db->order_by('ArticleId','desc');
        $query = $this->db->get("article_tbl");
        if ($query->num_rows() > 0) 
        {
            
            foreach ($query->result() as $row) 
            {

                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    public function editData($data,$articleId){
        $returnId=0;
        $this->db->where('ArticleId',$articleId);
        $returnId=$this->db->update('article_tbl',$data);
        return $returnId;
    }
    public function deleteData($articleId){
        $returnId=0;
        if($this->db->where('ArticleId',$articleId)){
            $returnId=$this->db->delete('article_tbl');
            return $returnId;
        }else{
            return $returnId;
        }
    }
}