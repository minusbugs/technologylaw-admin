<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!--<link rel="shortcut icon" href="../images/favicon.png" type="image/png">-->

  <title>Admin TLT</title>

  <link rel="stylesheet" href="<?=base_url()?>assets/lib/Hover/hover.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/lib/fontawesome/css/font-awesome.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/lib/weather-icons/css/weather-icons.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/lib/ionicons/css/ionicons.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/lib/jquery-toggles/toggles-full.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/quirk.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">
  <script src="<?=base_url()?>assets/lib/modernizr/modernizr.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>assets/lib/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.css">
  <link rel="icon" type="image/x-icon" href="assets/images/fav.ico" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="../lib/html5shiv/html5shiv.js"></script>
  <script src="../lib/respond/respond.src.js"></script>
  <![endif]-->
</head>

<body>

<header>
  <div class="headerpanel">

    <div class="logopanel">
      <h2><a href="index.html">TLT</a></h2>
    </div><!-- logopanel -->

    <div class="headerbar">

      <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>

      <div class="searchpanel">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search for...">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
          </span>
        </div><!-- input-group -->
      </div>

     
    </div><!-- headerbar -->
  </div><!-- header-->
</header>
