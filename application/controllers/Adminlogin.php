<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Adminlogin extends CI_Controller {
	public function __construct() {
    //load database in autoload libraries 
        parent::__construct(); 
        $this->load->helper('url_helper');
        $this->load->library('session'); 
        $this->load->helper('form'); 
        $this->load->model('Login_model');
        $this->load->model('newsandevent_model');  
        $this->load->model('articles_model');
        $this->load->library('pagination');
        $this->load->model('advertisement_model');
   }
	public function index(){
		if(isset($this->session->userdata['logged'])){
			if($this->session->userroll['admin']){
				echo "admin login";
			}else{
				redirect('Adminlogin/dashboard');
			}
		}
		else
		{
			$this->load->view('pages/signin');
		}
	}
	public function authentication(){
		//echo "authentication";
		$emailId=$this->input->post('emailid');
		$userPassword=$this->input->post('userpassword');
		if($emailId==NULL || $userPassword==NULL){
			echo "no data";
		}
		else{
			              
			 $result=$this->Login_model->login($emailId,$userPassword);
			if($result==NULL){
				echo "No Data";
			}
			else{
					$userRoll=$result->UserRoll;
					$userName=$result->UserName;
					$this->session->set_userdata('userEmail',$emailId);				
					$this->session->set_userdata('userName',$userName);
					$this->session->set_userdata('userRoll',$userRoll);
					$this->session->set_userdata('logged','Login');
					if($userRoll=='admin'){
						redirect('Adminlogin/dashboard');
					}else{
						echo "User view";
					}
					
			}		
		}
	}
	public function dashboard(){
		if(isset($this->session->userdata['logged'])){
			$data['tittle']="dashboard";
			$data['newsandeventcount']=$this->newsandevent_model->newsandeventcount();
			$data['advertisement']=$this->advertisement_model->advertisementCount();
			$data['article']=$this->articles_model->articleCount();
			$this->load->view('includes/header');
			$this->load->view('includes/leftpanel',$data);
			$this->load->view('pages/dashboard');
		}else{
			$this->load->view('pages/signin');
		}
	}
	public function getNewsandevents(){
		if(isset($this->session->userdata['logged'])){
			$data['tittle']="news&events";
			$data['status']='';
			$config = array();
			$config["base_url"] = base_url()."index.php/Adminlogin/getNewsandevents";
			$total_row = $this->newsandevent_model->newsandeventcount();
			$config["total_rows"] = $total_row;
			$config["per_page"] = 5;
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 5;
			$config['cur_tag_open'] = '&nbsp;<a class="current">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$this->pagination->initialize($config);
			if($this->uri->segment(3))
			{
				$page = ($this->uri->segment(3)) ;
			}
			else
			{
				$page = 0;
			}
			$data["events"] = $this->newsandevent_model->getDataOncreate($config["per_page"], $page);
			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );
			$this->load->view('includes/header');
			$this->load->view('includes/leftpanel',$data);
			$this->load->view('pages/newsandevents',$data);
		}else{
			$this->load->view('pages/signin');
		}
   	}
   	public function addNewsandvents(){
   		if(isset($this->session->userdata['logged'])){
   			$data['tittle']="news&events";
   			$data['status']='';	
   			$this->load->view('includes/header');
   			$this->load->view('includes/leftpanel',$data);
   			$this->load->view('pages/addnewsandevent');
   		}else{
   			$this->load->view('pages/signin');
   		}
   	}
   	public function insertNewsAndEvent(){
   		$time=time();
   		$eventTittle=$this->input->post('eventname');
   		$eventDescription=$this->input->post('eventdescription');
   		$eventDate=$this->input->post('eventdate');
   		$eventTime=$this->input->post('eventtime');
   		$image=$_FILES['eventimage']['name'];
   		$config['upload_path']          = './assets/newsandevent/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$images = explode('.',$image);
		$eventImage=$time.'.'.end($images);
		//$config['max_size']             = 2000;
		//$config['max_width']            = 1024;
		//$config['max_height']           = 768;
		$config['file_name'] = $eventImage;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('eventimage'))
		{
			$data=array('EventTittle' =>$eventTittle,
						'EventDescription'=>$eventDescription,
						'EventDate'=>$eventDate,
						'EventTime'=>$eventTime
			 		);
			$result=$this->newsandevent_model->insertData($data);
			if($result=='1'){
				$data['tittle']="news&events";	
				$data['status']="Success";
				$this->load->view('includes/header');
				$this->load->view('includes/leftpanel',$data);
				$this->load->view('pages/addnewsandevent',$data);
			}else{
				echo "some error";
				logg_error('error','failed to insert news&events to the database(Adminlogin/insertNewsAndEvent)');
			}	
		}else{
			$data=array('EventTittle' =>$eventTittle,
						'EventDescription'=>$eventDescription,
						'EventDate'=>$eventDate,
						'EventTime'=>$eventTime,
						'EventImage'=>$eventImage
			 		);
			$result=$this->newsandevent_model->insertData($data);
			if($result=='1'){
				$data['tittle']="news&events";	
				$data['status']="Updated Success";
				$this->load->view('includes/header');
				$this->load->view('includes/leftpanel',$data);
				$this->load->view('pages/addnewsandevent',$data);
			}else{
				echo "some error";
				logg_error('error','failed to insert news&events to the database(Adminlogin/insertNewsAndEvent)');
			}  
		} 
	     
   	}
   	public function editNewsandevents(){
   		echo "edit ";
   		$time=TIME();
   		$eventId=$this->input->post('eventid');
   		$eventTittle=$this->input->post('eventname');
   		$eventDescription=$this->input->post('eventdescription');
   		$eventDate=$this->input->post('eventdate');
   		$eventTime=$this->input->post('eventtime');
   		$image=$_FILES['eventimage']['name'];
   		$config['upload_path']          = './assets/newsandevent/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$images = explode('.',$image);
		$eventImage=$time.'.'.end($images);
		//$config['max_size']             = 2000;
		//$config['max_width']            = 1024;
		//$config['max_height']           = 768;
		$config['file_name'] = $eventImage;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload('eventimage'))
		{
			$data=array('EventTittle' =>$eventTittle,
						'EventDescription'=>$eventDescription,
						'EventDate'=>$eventDate,
						'EventTime'=>$eventTime
			 		);
			$result=$this->newsandevent_model->editData($data,$eventId);
			if($result=='1'){
				redirect('adminlogin/getNewsandevents');
			}else{
				echo "some error";
				log_message('error','failed to insert news&events to the database(Adminlogin/insertNewsAndEvent)');
				redirect('adminlogin/getNewsandevents');
			}	
		}
		else{
			$data=array('EventTittle' =>$eventTittle,
						'EventDescription'=>$eventDescription,
						'EventDate'=>$eventDate,
						'EventTime'=>$eventTime,
						'EventImage'=>$eventImage
			 		);
			$result=$this->newsandevent_model->editData($data,$eventId);
			if($result=='1'){
				redirect('adminlogin/getNewsandevents');
			}else{
				
				logg_error('error','failed to insert news&events to the database(Adminlogin/insertNewsAndEvent)');
				redirect('adminlogin/getNewsandevents');
			}  
		}

   	}
   	public function deleteNewsandevents(){
   		$eventId=$this->input->post('eventid');
   		$eventImage=$this->input->post('eventimage');
   		$result=$this->newsandevent_model->deleteData($eventId);
   		if($result=='1'){
   				redirect('Adminlogin/getNewsandevents');
   		}else{
   				redirect('Adminlogin/getNewsandevents');
   		}
   	}
	public function logout(){
		$this->session->unset_userdata('userName');
		$this->session->unset_userdata('userRoll');
		$this->session->unset_userdata('logged');
		$this->session->sess_destroy();
		redirect('Adminlogin/index');
	}
	public function addArticles($result){
		if(isset($this->session->userdata['logged'])){
			$data['tittle']="articles";
			if($result==1)
				$data['status']='Data Inserted';
			else if($result==2){
				$data['status']='Data Inseration Failed';
			}
			else{
				$data['status']="";
			}
			$this->load->view('includes/header');
			$this->load->view('includes/leftpanel',$data);
			$this->load->view('pages/addarticles',$data);
		}else{
			$this->load->view('pages/signin');
		}
	}
	public function articles(){
		if(isset($this->session->userdata['logged'])){
			$data['tittle']="articles";
			$data['status']='';
			$config = array();
			$config["base_url"] = base_url()."index.php/Adminlogin/articles";
			$total_row = $this->articles_model->articleCount();
			$config["total_rows"] = $total_row;
			$config["per_page"] = 5;
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 5;
			$config['cur_tag_open'] = '&nbsp;<a class="current">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$this->pagination->initialize($config);
			if($this->uri->segment(3))
			{
				$page = ($this->uri->segment(3)) ;
			}
			else
			{
				$page = 0;
			}
			$data["articles"] = $this->articles_model->getDataOncreate($config["per_page"], $page);
			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );
			$this->load->view('includes/header');
			$this->load->view('includes/leftpanel',$data);
			$this->load->view('pages/articles',$data);
		}
	}
	public function insertArticle(){
		$time=time();
		$articleTittle=$this->input->post('articlename');
		$articleDescription=$this->input->post('articledescription');
		$image=$_FILES['articleimage']['name'];
   		$config['upload_path']          = './assets/Articles/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$images = explode('.',$image);
		$articleImage=$time.'.'.end($images);
		//$config['max_size']             = 2000;
		//$config['max_width']            = 1024;
		//$config['max_height']           = 768;
		$config['file_name'] = $articleImage;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);	
		if ( ! $this->upload->do_upload('articleimage'))
		{
			$data=array('ArticleTittle' =>$articleTittle,
			'ArticleDescription'=>$articleDescription);
			$result=$this->articles_model->addData($data);
			if ($result==1) {
				redirect('Adminlogin/addArticles/A1');
			}else{
				echo "Data Not inseated";
			}
		}
		else{
			$data=array('ArticleTittle' =>$articleTittle,
			'ArticleDescription'=>$articleDescription,
			'ArticleImage'=>$articleImage);
			$result=$this->articles_model->addData($data);
			if ($result==1) {
				redirect('Adminlogin/addArticles/A1');
			}else{
				redirect('Adminlogin/addArticles/A2');
			}
		}
	}
	public function editArticle(){
		$time=time();
		$articleId=$this->input->post('articleid');
		$articleTittle=$this->input->post('articlename');
		$articleDescription=$this->input->post('articledescription');
		$image=$_FILES['articleimage']['name'];
   		$config['upload_path']          = './assets/Articles/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$images = explode('.',$image);
		$articleImage=$time.'.'.end($images);
		//$config['max_size']             = 2000;
		//$config['max_width']            = 1024;
		//$config['max_height']           = 768;
		$config['file_name'] = $articleImage;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);	
		if ( ! $this->upload->do_upload('articleimage'))
		{
			$data=array('ArticleTittle' =>$articleTittle,
			'ArticleDescription'=>$articleDescription);
			$result=$this->articles_model->editData($data,$articleId);
			if ($result==1) {
				redirect('Adminlogin/addArticles/A1');
			}else{
				echo "Data Not inseated";
			}
		}
		else{
			$data=array('ArticleTittle' =>$articleTittle,
			'ArticleDescription'=>$articleDescription,
			'ArticleImage'=>$articleImage);
			$result=$this->articles_model->editData($data,$articleId);
			if ($result==1) {
				redirect('Adminlogin/Articles/A1');
			}else{
				redirect('Adminlogin/articles/A3');
			}
		}
	}
	public function deleteArticle(){
		$articleId=$this->input->post('articleid');
		$articleImage=$this->input->post('articleimage');
		$result=$this->articles_model->deleteData($articleId);
		if($result==1){
			redirect('adminlogin/articles/2');
		}else{
			echo('adminlogin/articles/3');
		}
	}
	public function changePassword(){
		if(isset($this->session->userdata['logged'])){
			$data['tittle']='settings';
			$data['status']='';
			$this->load->view('includes/header');
			$this->load->view('includes/leftpanel',$data);
			$this->load->view('pages/changepassword');
		}else{
			redirect('Adminlogin/index');
		}
	}
	public function updatePassword(){
		$newPassword1=$this->input->post('newpassword1');
		$newPassword2=$this->input->post('newpassword2');
		$oldPassword=$this->input->post('oldpassword');
		if($newPassword2!=NULL && $newPassword2!=NULL && $oldPassword!=NULL ){
			if($newPassword1==$newPassword2){
				$emailId=$this->session->userEmail;
				$result=$this->Login_model->login($emailId,$oldPassword);
				if($result==NULL){
					$data['tittle']='settings';
					$data['status']="old password is wrong";
					$this->load->view('includes/header');
					$this->load->view('includes/leftpanel',$data);
					$this->load->view('pages/changepassword');
				}else{
					$data=array('UserPassword' =>$newPassword2);
					$result=$this->Login_model->changePassword($data,$emailId);
					if($result==NULL){
						logg_error('Error','Change Password ');
					}else{
						$data['tittle']='settings';
						$data['status']="Password Changed";
						$this->load->view('includes/header');
						$this->load->view('includes/leftpanel',$data);
						$this->load->view('pages/changepassword');
					}
				}
			}else{
				$data['tittle']='settings';
				$data['status']="New Password are Different";
				$this->load->view('includes/header');
				$this->load->view('includes/leftpanel',$data);
				$this->load->view('pages/changepassword');
			}
		}else{
			$data['tittle']='settings';
			$data['status']="enter the password";
			$this->load->view('includes/header');
			$this->load->view('includes/leftpanel',$data);
			$this->load->view('pages/changepassword');
		}
	}
	public function advertisement(){
		if(isset($this->session->userdata['logged'])){
			$config = array();
			$config["base_url"] = base_url()."index.php/Adminlogin/advertisement";
			$total_row = $this->advertisement_model->advertisementCount();
			$config["total_rows"] = $total_row;
			$config["per_page"] = 12;
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 5;
			$config['cur_tag_open'] = '&nbsp;<a class="current">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$this->pagination->initialize($config);
			if($this->uri->segment(3))
			{
				$page = ($this->uri->segment(3)) ;
			}
			else
			{
				$page = 0;
			}
			$data["advertisement"] = $this->advertisement_model->getDataOncreate($config["per_page"], $page);
			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );

			// print_r($data);exit;
			$data['tittle']="advertisement";
			$this->load->view('includes/header');
			$this->load->view('includes/leftpanel',$data);
			$this->load->view('pages/advertisement',$data);
		}
	}
	public function addAdvertisement($status){
		if (isset($this->session->userdata['logged'])) {
			$data['tittle']="advertisement";
			if($status=='1'){
				$data['status']="Data Inserted";
			}else if($status=='2'){
				$data['status']="Data Insrtion Failed Try again";
			}else{
				$data['status']='';
			}
			$this->load->view('includes/header');
			$this->load->view('includes/leftpanel',$data);
			$this->load->view('pages/addadvertisement');
		}
	}
	public function insertAdvertisement(){
		$time=time();
		$url=$this->input->post('url');
		$image=$_FILES['adsimage']['name'];
   		$config['upload_path']          = './assets/Advertisement/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$images = explode('.',$image);
		$adsImage=$time.'.'.end($images);
		//$config['max_size']             = 2000;
		//$config['max_width']            = 1024;
		//$config['max_height']           = 768;
		$config['file_name'] = $adsImage;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);	
		if ( ! $this->upload->do_upload('adsimage'))
		{
			log_message('error','failed to insert advertisementimage in insertAdvertisement');
				redirect('Adminlogin/addAdvertisement/3');
		}
		else{
			$data=array('AdvertisementImage'=>$adsImage,
						'AdvertisementUrl'=>$url
						);
			$result=$this->advertisement_model->addData($data);
			if ($result==1) {
				redirect('Adminlogin/addAdvertisement/1');
			}else{
				redirect('Adminlogin/addAdvertisement/2');
			}
		}
	}
	public function editAdvertisement(){
		$time=time();
		$advertisementUrl=$this->input->post('advertisementurl');
		$advertisementId=$this->input->post('advertisementid');
		$image=$_FILES['advertisementimage']['name'];

		if($advertisementUrl!=null || $image!=null){
	   		$config['upload_path']          = './assets/advertisement/';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$images = explode('.',$image);
			$advertisementImage=$time.'.'.end($images);
			//$config['max_size']             = 2000;
			//$config['max_width']            = 1024;
			//$config['max_height']           = 768;
			$config['file_name'] = $advertisementImage;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);	

			if( ! $this->upload->do_upload('advertisementimage'))
			{
				$data=array('AdvertisementUrl'=>$advertisementUrl
							);
				echo $advertisementId;
				$result=$this->advertisement_model->editData($data,$advertisementId);
				if ($result==1) {
					redirect('Adminlogin/advertisement');
				}else{
					echo "Data Not inseated";	
				}
			}
			else{
				$data=array('AdvertisementUrl'=>$advertisementUrl,
							'AdvertisementImage'=>$advertisementImage
							);
				$result=$this->advertisement_model->editData($data,$advertisementId);
				if ($result==1) {
					redirect('Adminlogin/advertisement');
				}else{
					echo "failed";exit;
					redirect('Adminlogin/advertisement');
				}
			}
		}else{
			echo "enter minimum one value for updation";exit;
		}
	}
	public function deleteAdvertisement(){
		$advertisementid=$this->input->post('advertisementid');
		$advertisementImage=$this->input->post('advertisementimage');
		$result=$this->advertisement_model->deleteData($advertisementid);
		if($result==1){
			redirect('adminlogin/advertisement');
		}else{
			echo('adminlogin/articles/3');
		}
	}

}