<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class advertisement_model extends CI_Model{
	 function __construct()
    {
       $this->load->database();
    }
	  public function advertisementCount(){
        return $this->db->count_all("advertisement_tbl");
    }
     public function addData($data){
    	$returnId=0;
        $returnId=$this->db->insert('advertisement_tbl',$data);
        return $returnId;
    }
    public function getDataOncreate($limit, $id){
        if($id>1)
        {
            $offset = ($id-1)*$limit;
            $this->db->limit($limit,$offset);
        }else
        {
            $this->db->limit($limit,$id);
        }
        $this->db->order_by('AdvertisementId','desc');
        $query = $this->db->get("advertisement_tbl");
        if ($query->num_rows() > 0) 
        {
            
            foreach ($query->result() as $row) 
            {

                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
     public function editData($data,$advertisementId){
        $returnId=0;
        $this->db->where('AdvertisementId',$advertisementId);
        $returnId=$this->db->update('advertisement_tbl',$data);
        return $returnId;
    }
    public function deleteData($advertisementId){
        $returnId=0;
        if($this->db->where('AdvertisementId',$advertisementId)){
            $returnId=$this->db->delete('advertisement_tbl');
            return $returnId;
        }else{
            return $returnId;
        }
    }
}