<?php
	$this->load->view('includes/header');
?>
<section>
	  <div class="mainpanel">
	    <div class="contentpanel">
			<ol class="breadcrumb breadcrumb-quirk">
				<li><a href="<?=base_url()?>"><i class="fa fa-home mr5"></i> Home</a></li>
				<li>Settings</a></li>
				<li class="active">Change Pasword</li>
			</ol>
			<?php
	              	if($status!=NULL){
	              ?>
					<div class="alert alert-info alert-margin">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<strong><?=$status?></strong>
					</div>
	              <?php
	              	}	
	              ?>
			<div class="panel">
				<div class="panel-heading">
					<h4 class="panel-title">Change Password</h4>
				</div>
				<?=form_open_multipart('adminlogin/updatePassword')?>
				<div class="panel-body">
					<div class="form-group ">
						<input type="text" name="newpassword1" class="form-control" placeholder="New password">
					</div>
					<div class="form-group ">
						<input type="text" name="newpassword2"class="form-control" placeholder="Re-enter new password">
					</div>
					<div class="form-group">
						<input type="text" name="oldpassword" class="form-control" placeholder="Old password">
					</div>
					<div class=" form-group  col-sm-offset-10">
						<div class="col-sm-9 col-sm-offset-3">
                      <input type="submit" class="btn btn-success btn-quirk btn-wide mr5" value="Submit">
                    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
	echo form_close();
	$this->load->view('includes/footer');
?>