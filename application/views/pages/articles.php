<?php
	$this->load->view('includes/header.php');
?>
<section>
    <div class="mainpanel">

    <div class="contentpanel">

      <ol class="breadcrumb breadcrumb-quirk">
        <li><a href="<?=site_url()?>/adminlogin/dashboard"><i class="fa fa-home mr5"></i> Home</a></li>
        <li class="active">Articles</li>
      </ol>
        <div class="panel-heading 1 " >
          <h1 class="panel-title" align="center">Articles</h1>
          <?php
          if($status!=NULL){
          ?>
          <div class="alert alert-info alert-margin">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong><?=$status?></strong>
          </div>
          <?php
          }
          ?>
        </div>
      <div class="row">
        <?php
          foreach ($articles as $a){ 
        ?>
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"><?=$a->ArticleTittle?></h4>
              <ul class="panel-options">
                <div class="btn-group fm-group">
                  <button type="button" class="btn btn-default dropdown-toggle fm-toggle" data-toggle="dropdown">
                  <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu pull-right fm-menu" role="menu">
                    <li data-toggle="modal" data-target="#editModal<?=$a->ArticleId?>"><a href="#"><i class="fa fa-pencil"></i> Edit</a></li>
                    <li data-toggle="modal" data-target="#deleteModal<?=$a->ArticleId?>"><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                  </ul>
                </div>
              </ul>
            </div>
            <div class="panel-body">
              <div class="media">
                <div class="media-left">
                  <?php
                    if($a->ArticleImage!=NULL){
                  ?>
                    <img class="media-object width80" src="<?=base_url()?>assets/Articles/<?=$a->ArticleImage?>" alt="">
                  <?php
                    }
                  ?>
                      <?=$a->ArticleDescription?>
                </div>
              </div>
            </div>
          </div><!-- panel -->
        </div><!-- col-md-12 -->

        <div class="modal bounceIn animated" id="editModal<?=$a->ArticleId?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" align="center">Edit Article</h4>
              </div>
              <div class="modal-body">
            <div id="basicForm"  class="form-horizontal">
            <?=form_open_multipart('adminlogin/editArticle');?>
              <div class="form-group">
                <input type="hidden" name="articleid" value="<?=$a->ArticleId?>">
                <label class="col-sm-3 control-label">Article Name <span class="text-danger">*</span></label>
                <div class="col-sm-8">
                  <input type="text" name="articlename" class="form-control" value="<?=$a->ArticleTittle?>"required />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Article Descreption<span class="text-danger">*</span></label>
                <div class="col-sm-8">
                  <textarea id="wysiwyg"  name="articledescription"placeholder="Enter text here..." class="form-control wys" rows="10"><?=$a->ArticleDescription?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-3 control-label">Article Image <span class="text-danger">*</span></label>
                <div class="col-sm-8">
                  <input type="file" name="articleimage" class="form-control" "required />
                </div>
              </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Save changes" />
                <?=form_close()?>
                </div>
              </div>
            </div><!-- modal-content -->
          </div><!-- modal-dialog -->
        </div><!-- modal -->


        <div class="modal bounceIn animated" id="deleteModal<?=$a->ArticleId?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              </div>
              <div class="modal-body ">
                 <h4 class="text-color">Do you Want Delete this...</h4>
              </div>
              <?=form_open('Adminlogin/deleteArticle')?>
              <input type="hidden" name="articleid" value="<?=$a->ArticleId?>">
              <input type="hidden" name="articleimage" value="<?=$a->ArticleImage?>">
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary" value="Delete" />
              </div>
              <?=form_close()?>
            </div><!-- modal-content -->
          </div><!-- modal-dialog -->
        </div><!-- modal -->
        <?php
          }
        ?>
      </div><!-- row -->
    </div>
  </div>
</section>	
<ul class="pagination pull-right">
        <?php foreach ($links as $link) 
        {
          ?><li><?php echo $link?></li>
          <?php }
        ?>
      </ul>
<?php
	$this->load->view('includes/footer.php');
?>
<script>
$(document).ready(function(){

  'use strict';

  // HTML5 WYSIWYG Editor
  $('.wys').wysihtml5({
    
    toolbar: {
      fa: false,
      "image": false,
    }
  });
});
</script>