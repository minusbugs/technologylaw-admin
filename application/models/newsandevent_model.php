<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsandevent_model extends CI_Model{
	 function __construct()
    {
       $this->load->database();
    }
    public function newsandeventcount(){
        return $this->db->count_all('newsandevent_tbl');
    }
    public function getDataOncreate($limit,$id){
        if($id>1){
            $offset=($id-1)*$limit;
            $this->db->limit($limit,$offset);
        }else{
            $this->db->limit($limit,$id);
        }
    	$this->db->order_by('EventId','desc');
    	$query=$this->db->get('newsandevent_tbl');
    	if ($query->num_rows() > 0) 
        {
            
            foreach ($query->result() as $row) 
            {

                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    public function insertData($data){
        $returnId=0;
        $returnId=$this->db->insert('newsandevent_tbl',$data);
        return $returnId;
    }
    public function editData($data,$eventId){
        $returnId=0;
        $this->db->where('EventId',$eventId);
        $returnId=$this->db->update('newsandevent_tbl',$data);
        return $returnId;
    }
    public function deleteData($eventId){

        $returnId=0;
        if($this->db->where('EventId',$eventId)){
            $returnId=$this->db->delete('newsandevent_tbl');
            return $returnId;
        }else{
          return $returnId='0';
        }
    }

}