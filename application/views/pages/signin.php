<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!--<link rel="shortcut icon" href="<?=base_url()?>assets/images/favicon.png" type="image/png">-->
 <link rel="icon" type="image/x-icon" href="assets/images/fav.ico" />
  <title>TLT-Admin</title>

  <link rel="stylesheet" href="<?=base_url()?>assets/lib/fontawesome/css/font-awesome.css">

  <link rel="stylesheet" href="<?=base_url()?>assets/css/quirk.css">

  <script src="<?=base_url()?>assets/lib/modernizr/modernizr.js"></script>
<body class="signwrapper">

  <div class="sign-overlay"></div>
  <div class="signpanel"></div>

  <div class="panel signin">
    <div class="panel-heading">
      <h1>TLT</h1>
      <h4 class="panel-title">Welcome! Please signin.</h4>
    </div>
    <div class="panel-body">
     
      
      <?=form_open('adminlogin/authentication')?>
        <div class="form-group mb10">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input type="text" class="form-control" placeholder="Enter your email id" name="emailid" id="emailid">
          </div>
        </div>
        <div class="form-group nomargin">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input type="text" class="form-control" placeholder="Enter Password" name="userpassword" id="userpassword">
          </div>
        </div>
        <div><a href="" class="forgot">Forgot password?</a></div>
        <div class="form-group">
          <input type="submit" value="Sign In" name="login_btn" class="btn btn-success btn-quirk btn-block">
        </div>
      </form>
      <hr class="invisible">
      <div class="form-group">
       
      </div>
    </div>
  </div><!-- panel -->
  <?=form_close()?>
  <!-- <form method="POST" action="<?=base_url()?>apicontroller/authentication"> -->