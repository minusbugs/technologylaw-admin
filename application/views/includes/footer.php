</section>

<script src="<?=base_url()?>assets/lib/jquery/jquery.js"></script>
<script src="<?=base_url()?>assets/lib/jquery-ui/jquery-ui.js"></script>
<script src="<?=base_url()?>assets/lib/bootstrap/js/bootstrap.js"></script>
<script src="<?=base_url()?>assets/lib/jquery-toggles/toggles.js"></script>

<!-- <script src="<?=base_url()?>assets/lib/morrisjs/morris.js"></script> -->
<script src="<?=base_url()?>assets/lib/raphael/raphael.js"></script>

<script src="<?=base_url()?>assets/lib/flot/jquery.flot.js"></script>
<script src="<?=base_url()?>assets/lib/flot/jquery.flot.resize.js"></script>
<script src="<?=base_url()?>assets/lib/flot-spline/jquery.flot.spline.js"></script>

<script src="<?=base_url()?>assets/lib/jquery-knob/jquery.knob.js"></script>

<script src="<?=base_url()?>assets/js/quirk.js"></script>
<!-- <script src="<?=base_url()?>assets/js/dashboard.js"></script> -->

<script src="<?=base_url()?>assets/lib/wysihtml5x/wysihtml5x.js"></script>
<script src="<?=base_url()?>assets/lib/wysihtml5x/wysihtml5x-toolbar.js"></script>
<script src="<?=base_url()?>assets/lib/handlebars/handlebars.js"></script>
<script src="<?=base_url()?>assets/lib/summernote/summernote.js"></script>
<script src="<?=base_url()?>assets/lib/bootstrap3-wysihtml5-bower/bootstrap3-wysihtml5.all.js"></script>

</body>
</html>
