<?php
	$this->load->view('includes/header.php');
?>
<section>
	<div class="mainpanel">

      <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
          <li><a href="<?=base_url()?>"><i class="fa fa-home mr5"></i> Home</a></li>
          <li>News&Events</li>
        </ol>

        <hr class="darken">
        	<div class="panel-heading 1 " >
                	<h1 class="panel-title" align="center"> News&Events</h1>
	              <?php
	              	if($status!=NULL){
	              ?>
					<div class="alert alert-info alert-margin">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<strong><?=$status?></strong>
					</div>
	              <?php
	              	}	
	              ?>
              </div>
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-10">
            <div class="timeline-wrapper">
            <?php
        		foreach ($events as $e) {
       		 ?>
              <div class="timeline-date"><?=$e->EventDate?></div>

              <div class="panel panel-post-item status">
                <div class="panel-heading">
                  <div class="media">
                    <div class="media-left">
                    <?php
                    	if ($e->EventImage!=NULL) {
                    ?>
                      <a href="#">
                        <img alt="" src="<?=base_url()?>assets/newsandevent/<?=$e->EventImage?>" class="media-object img-circle">
                      </a>
                    <?php
                    	}else{
                    ?>
							<a href="#">
								<img alt="" src="<?=base_url()?>assets/images/photos/user1.png?>" class="media-object img-circle">
                      </a>
                    <?php
                    	}
                    ?>
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading"><?=$e->EventTittle?></h4>
                      <p class="media-usermeta">
                        <span class="media-time"><?=$e->EventTime?></span>
                        <a href=""><i class="glyphicon glyphicon-map-marker"></a></i> <a href="">Kochi</a>
                      </p>
                    </div>
                  </div><!-- media -->
                  <ul class="panel-options">
					<div class="btn-group fm-group">
						<button type="button" class="btn btn-default dropdown-toggle fm-toggle" data-toggle="dropdown">
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu pull-right fm-menu" role="menu">
							<li data-toggle="modal" data-target="#editModal<?=$e->EventId?>"><a href="#"><i class="fa fa-pencil"></i> Edit</a></li>
							<li data-toggle="modal" data-target="#deleteModal<?=$e->EventId?>"><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
						</ul>
					</div>
                  </ul>
                </div><!-- panel-heading -->
                <div class="panel-body">
                 	<?=$e->EventDescription?>
                </div>
    
              </div>

				<div class="modal bounceIn animated" id="editModal<?=$e->EventId?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				        <h4 class="modal-title" id="myModalLabel" align="center">Edit News & Events</h4>
				      </div>
				      <div class="modal-body">
						<div id="basicForm"  class="form-horizontal">
						<?=form_open_multipart('adminlogin/editNewsandevents');?>
							<div class="form-group">
								<input type="hidden" name="eventid" value="<?=$e->EventId?>">
								<label class="col-sm-3 control-label">Event Name <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="eventname" class="form-control" value="<?=$e->EventTittle?>"required />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Event Descreption<span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<textarea rows="5" name="eventdescription" class="form-control" required><?=$e->EventDescription?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Event Date <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="eventdate" class="form-control" value="<?=$e->EventDate?>"required />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Event Time <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<input type="text" name="eventtime" class="form-control" value="<?=$e->EventTime?>"required />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Event Image <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<input type="file" name="eventimage" class="form-control" "required />
								</div>
							</div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <input type="submit" class="btn btn-primary" value="Save changes" />
				        <?=form_close()?>
				        </div>
				      </div>
				    </div><!-- modal-content -->
				  </div><!-- modal-dialog -->
				</div><!-- modal -->

				<div class="modal bounceIn animated" id="deleteModal<?=$e->EventId?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header ">
				        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				      </div>
				      <div class="modal-body ">
				         <h4 class="text-color">Do you Want Delete this...</h4>
				      </div>
				      <?=form_open('Adminlogin/deleteNewsandevents')?>
				      <input type="hidden" name="eventid" value="<?=$e->EventId?>">
				      <input type="hidden" name="eventimage" value="<?=$e->EventImage?>">
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <input type="submit" class="btn btn-primary" value="Delete" />
				      </div>
				      <?=form_close()?>
				    </div><!-- modal-content -->
				  </div><!-- modal-dialog -->
				</div><!-- modal -->
              <?php }?>             
            </div><!-- timeline-wrapper -->
          </div>
        </div><!-- row -->

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->		
</section>
<ul class="pagination pull-right">
        <?php foreach ($links as $link) 
        {
          ?><li><?php echo $link?></li>
          <?php }
        ?>
      </ul>
<?php
	$this->load->view('includes/footer.php');
?>