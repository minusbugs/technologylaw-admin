<?php
	$this->load->view('includes/header');
?>
<section>
	<div class="mainpanel">

      <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
          <li><a href="<?=site_url()?>/adminlogin/dashboard"><i class="fa fa-home mr5"></i> Home</a></li>
          <li><a href="<?=site_url()?>/adminlogin/getNewsandevents">News&events</a></li>
          <li class="active">Add News&Events</li>
        </ol>

       <div class="col-md-12">
          <div class="panel">
              <div class="panel-heading nopaddingbottom " >
                	<h1 class="panel-title" align="center">Add News&Events</h1>
	              <?php
	              	if($status!=NULL){
	              ?>
					<div class="alert alert-info alert-margin">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<strong>Data Inserted Success</strong>
					</div>
	              <?php
	              	}	
	              ?>
              </div>
             	
              <div class="panel-body">
                <hr>
                <div class="form-horizontal">
                <?=form_open_multipart('adminlogin/insertNewsAndEvent');?>
				<div class="form-group">
					<input type="hidden" name="eventid">
					<label class="col-sm-3 control-label">Event Name <span class="text-danger">*</span></label>
					<div class="col-sm-8">
						<input type="text" name="eventname" class="form-control" required />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Event Descreption<span class="text-danger">*</span></label>
					<div class="col-sm-8">
						<textarea rows="5" name="eventdescription" class="form-control" required></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Event Date <span class="text-danger">*</span></label>
					<div class="col-sm-8">
						<input type="text" name="eventdate" class="form-control" required />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Event Time <span class="text-danger">*</span></label>
					<div class="col-sm-8">
						<input type="text" name="eventtime" class="form-control" required />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Event Image <span class="text-danger"></span></label>
					<div class="col-sm-8">
						<input type="file" name="eventimage" class="form-control" />
					</div>
				</div>
                  <hr>

                  <div class="row">
                    <div class="col-sm-9 col-sm-offset-3">
                      <input type="submit" class="btn btn-success btn-quirk btn-wide mr5" value="Submit"></button>
                    </div>
                  </div>
                  <?=form_close()?>
                </div> 
              </div><!-- panel-body -->
          </div><!-- panel -->

        </div>

      </div><!-- contentpanel -->
    </div><!-- mainpanel -->		
</section>
<?php
	$this->load->view('includes/footer');
?>