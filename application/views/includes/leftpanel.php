<?php
  $this->load->helper('url_helper');
?>
<div class="leftpanel">
    <div class="leftpanelinner">

      <!-- ################## LEFT PANEL PROFILE ################## -->

      <div class="media leftpanel-profile">
        <div class="media-left">
          <a href="#">
            <img src="<?=base_url()?>assets/images/photos/loggeduser.png" alt="" class="media-object img-circle">
          </a>
        </div>
        <div class="media-body">
          <h4 class="media-heading">Admin<a data-toggle="collapse" class="pull-right"><i class="fa fa-angle-down"></i></a></h4>
          <span></span>
        </div>
      </div><!-- leftpanel-profile -->

      

      <ul class="nav nav-tabs nav-justified nav-sidebar">
        <li class="tooltips <?php if($tittle!='settings')echo 'active'; ?>" data-toggle="tooltip" title="Main Menu"><a data-toggle="tab" data-target="#mainmenu"><i class="tooltips fa fa-ellipsis-h"></i></a></li>
      
        <li class="tooltips" data-toggle="tooltip" title="Log Out"><a href="<?=site_url()?>/Adminlogin/logout"><i class="fa fa-sign-out"></i></a></li>
      </ul>

      <div class="tab-content">

        <!-- ################# MAIN MENU ################### -->

        <div class="tab-pane <?php if($tittle!='settings')echo 'active';?>" id="mainmenu">
          <h5 class="sidebar-title">Favorites</h5>
          <ul class="nav nav-pills nav-stacked nav-quirk">
            <li <?php if($tittle=='dashboard')echo "class=active";?>><a href="<?=site_url()?>/Adminlogin/dashboard"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
            <li <?php if($tittle=='news&events')echo "class=active";else echo "class=nav-parent";?>>
              <a href="#"><i class="fa fa-calendar"></i> <span>News & Events</span></a>
              <ul class="children">
                <li><a href="<?=site_url()?>/adminlogin/getNewsandevents/0">News & Events</a></li>
                <li><a href="<?=site_url()?>/adminlogin/addNewsandvents">Add News & Events</a></li>
              </ul>
            </li>
             <li <?php if($tittle=='articles')echo "class=active";else echo "class=nav-parent";?>>
              <a href="#"><i class="fa fa-newspaper-o"></i> <span>Articles</span></a>
              <ul class="children">
                <li><a href="<?=site_url()?>/adminlogin/articles/0">Articles</a></li>
                <li><a href="<?=site_url()?>/adminlogin/addArticles/0">Add Articles</a></li>
              </ul>
            </li>
             <li <?php if($tittle=='advertisement')echo "class=active";else echo "class=nav-parent";?>>
              <a href="#"><i class="fa fa-buysellads"></i> <span>Advertisement</span></a>
              <ul class="children">
                <li><a href="<?=site_url()?>/adminlogin/advertisement/0">Advertisement</a></li>
                <li><a href="<?=site_url()?>/adminlogin/addAdvertisement/0">Add Advertisement</a></li>
              </ul>
            </li>
           </ul>
        </div><!-- tab-pane -->

       

        <!-- #################### SETTINGS ################### -->

        <div class="tab-pane <?php if($tittle=='settings')echo 'active';?> " id="settings">
          <h5 class="sidebar-title">Security Settings</h5>
          <ul class="list-group list-group-settings">  
              <ul class="nav nav-pills nav-stacked nav-quirk">
            <li <?php if($tittle=='settings')echo "class=active";?>><a href="<?=site_url()?>/Adminlogin/changePassword"><i class=""></i> <span>Change Password</span></a></li>
           </ul>          
          </ul>
        </div><!-- tab-pane -->


      </div><!-- tab-content -->

    </div><!-- leftpanelinner -->
  </div><!-- leftpanel -->
