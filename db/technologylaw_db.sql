-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2018 at 03:25 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `technologylaw_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertisement_tbl`
--

CREATE TABLE `advertisement_tbl` (
  `AdvertisementId` int(11) NOT NULL,
  `AdvertisementImage` text NOT NULL,
  `AdvertisementUrl` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advertisement_tbl`
--

INSERT INTO `advertisement_tbl` (`AdvertisementId`, `AdvertisementImage`, `AdvertisementUrl`) VALUES
(3, 'karnataka.jpg', 'hbsdfbhsdsb'),
(4, '1537268452.jpg', 'www.what.com'),
(5, '1537268417.jpg', 'hserthtrh');

-- --------------------------------------------------------

--
-- Table structure for table `article_tbl`
--

CREATE TABLE `article_tbl` (
  `ArticleId` int(11) NOT NULL,
  `ArticleTittle` longtext NOT NULL,
  `ArticleDescription` longtext NOT NULL,
  `ArticleImage` varchar(200) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `article_tbl`
--

INSERT INTO `article_tbl` (`ArticleId`, `ArticleTittle`, `ArticleDescription`, `ArticleImage`, `CreatedOn`) VALUES
(14, 'Data Protection Law Series- I European Union General Data Protection Regulation- A Primer', 'History of General Data Protection Regulation starts with EC Directive 95/46/EC (herein after �the Directive�). The Directive continued to dominate the data protection domain in European Union for about two decades. However, in 2011, European Data Protection Supervisor sought European Commission�s opinion on bringing about a comprehensive approach on personal data protection in European Union. In 2012, EC Commission proposed a comprehensive reform of the Directive to strengthen online privacy rights and boost Europe�s digital economy. This proposal was adopted as an opinion. In 2014, European Parliament voted in favour of General Data Protection Regulation and in 2015, European Commission reached a general approach on it. It sought recommendations for GDPR. By the end of 2015, the European Parliament, European Council and European Commission reached an agreement on GDPR. In 2016, Article 29 Working Party issues an action plan for the implementation of GDPR. On 27th April, 2016 European Parliament and European Council adopted Regulation 2016/679 on the protection of natural persons with regard to the processing of personal data and on the free movement of such data and to repeal the Directive[1].\r\n\r\nAbout the General Data Protection Regulation\r\n\r\nThe General Data Protection Regulation is built around two Pillars � 1) Data protection of �natural persons� as Fundamental Rights; and 2) free flow of personal data and processing between Member States.\r\n\r\nGDPR applies to all companies processing the personal data of data subjects residing in the Union, regardless of the company�s location. It will apply to the processing of personal data by controllers and processors in the EU, regardless of whether the processing takes place in the EU or not. Consent of the data subject will have to be obtained in �intelligible and easily accessible form, with the purpose for data processing attached to that consent�.', 'http://templates.stillidea.net/investigate/images/resource/blog-detail.jpg', '2018-09-13 09:14:46'),
(15, 'Data Protection ', 'History of General Data Protection Regulation starts with EC Directive 95/46/EC (herein after �the Directive�). The Directive continued to dominate the data protection domain in European Union for about two decades. However, in 2011, European Data Protection Supervisor sought European Commission�s opinion on bringing about a comprehensive approach on personal data protection in European Union. In 2012, EC Commission proposed a comprehensive reform of the Directive to strengthen online privacy rights and boost Europe�s digital economy. This proposal was adopted as an opinion. In 2014, European Parliament voted in favour of General Data Protection Regulation and in 2015, European Commission reached a general approach on it. It sought recommendations for GDPR. By the end of 2015, the European Parliament, European Council and European Commission reached an agreement on GDPR. In 2016, Article 29 Working Party issues an action plan for the implementation of GDPR. On 27th April, 2016 European Parliament and European Council adopted Regulation 2016/679 on the protection of natural persons with regard to the processing of personal data and on the free movement of such data and to repeal the Directive[1].\r\n\r\nAbout the General Data Protection Regulation\r\n\r\nThe General Data Protection Regulation is built around two Pillars � 1) Data protection of �natural persons� as Fundamental Rights; and 2) free flow of personal data and processing between Member States.\r\n\r\nGDPR applies to all companies processing the personal data of data subjects residing in the Union, regardless of the company�s location. It will apply to the processing of personal data by controllers and processors in the EU, regardless of whether the processing takes place in the EU or not. Consent of the data subject will have to be obtained in �intelligible and easily accessible form, with the purpose for data processing attached to that consent�.', '', '2018-09-13 09:14:46'),
(17, 'Data Protection ', 'History of General Data Protection Regulation starts with EC Directive 95/46/EC (herein after �the Directive�). The Directive continued to dominate the data protection domain in European Union for about two decades. However, in 2011, European Data Protection Supervisor sought European Commission�s opinion on bringing about a comprehensive approach on personal data protection in European Union. In 2012, EC Commission proposed a comprehensive reform of the Directive to strengthen online privacy rights and boost Europe�s digital economy. This proposal was adopted as an opinion. In 2014, European Parliament voted in favour of General Data Protection Regulation and in 2015, European Commission reached a general approach on it. It sought recommendations for GDPR. By the end of 2015, the European Parliament, European Council and European Commission reached an agreement on GDPR. In 2016, Article 29 Working Party issues an action plan for the implementation of GDPR. On 27th April, 2016 European Parliament and European Council adopted Regulation 2016/679 on the protection of natural persons with regard to the processing of personal data and on the free movement of such data and to repeal the Directive[1].\r\n\r\nAbout the General Data Protection Regulation\r\n\r\nThe General Data Protection Regulation is built around two Pillars � 1) Data protection of �natural persons� as Fundamental Rights; and 2) free flow of personal data and processing between Member States.\r\n\r\nGDPR applies to all companies processing the personal data of data subjects residing in the Union, regardless of the company�s location. It will apply to the processing of personal data by controllers and processors in the EU, regardless of whether the processing takes place in the EU or not. Consent of the data subject will have to be obtained in �intelligible and easily accessible form, with the purpose for data processing attached to that consent�.', '', '2018-09-13 09:14:46'),
(18, 'Data Protection Law Series- I European Union General Data Protection Regulation- A Primer', 'History of General Data Protection Regulation starts with EC Directive 95/46/EC (herein after �the Directive�). The Directive continued to dominate the data protection domain in European Union for about two decades. However, in 2011, European Data Protectio�.', '', '2018-09-13 09:14:46'),
(19, 'Data Protection Law Series- I European Union General Data Protection Regulation- A Primer', 'History of General Data Protection Regulation starts with EC Directive 95/46/EC (herein after �the Directive�). The Directive continued to dominate the data protection domain in European Union for about two decades. However, in 2011, European Data Protectio�.', '', '2018-09-13 09:14:46'),
(20, 'Data Protection Law Series- I European Union General Data Protection Regulation- A Primer', 'History of General Data Protection Regulation starts with EC Directive 95/46/EC (herein after �the Directive�). The Directive continued to dominate the data protection domain in European Union for about two decades. However, in 2011, European Data Protectio�.', '', '2018-09-13 09:14:46'),
(21, 'Data Protection Law Series- I European Union General Data Protection Regulation- A Primer', 'History of General Data Protection Regulation starts with EC Directive 95/46/EC (herein after �the Directive�). The Directive continued to dominate the data protection domain in European Union for about two decades. However, in 2011, European Data Protectio�.', '', '2018-09-13 09:14:46');

-- --------------------------------------------------------

--
-- Table structure for table `login_tbl`
--

CREATE TABLE `login_tbl` (
  `UserId` int(11) NOT NULL,
  `UserName` varchar(30) NOT NULL,
  `UserPassword` varchar(30) NOT NULL,
  `EmailId` varchar(40) NOT NULL,
  `ContactNumber` varchar(13) NOT NULL,
  `UserRoll` varchar(6) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_tbl`
--

INSERT INTO `login_tbl` (`UserId`, `UserName`, `UserPassword`, `EmailId`, `ContactNumber`, `UserRoll`, `CreatedOn`) VALUES
(1, 'admin', 'a', 'admin@gmail.com', '9539173787', 'admin', '2018-09-04 07:08:15');

-- --------------------------------------------------------

--
-- Table structure for table `newsandevent_tbl`
--

CREATE TABLE `newsandevent_tbl` (
  `EventId` int(11) NOT NULL,
  `EventTittle` varchar(50) NOT NULL,
  `EventDescription` text NOT NULL,
  `EventDate` varchar(15) NOT NULL,
  `EventTime` varchar(11) NOT NULL,
  `IsAttachement` int(11) NOT NULL,
  `EventImage` varchar(50) NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsandevent_tbl`
--

INSERT INTO `newsandevent_tbl` (`EventId`, `EventTittle`, `EventDescription`, `EventDate`, `EventTime`, `IsAttachement`, `EventImage`, `CreatedOn`) VALUES
(1, 'Dummy', 'kjfhw ewljfhwehf wefklwelf wef.wejflwkef we.gfjwerlfgw rgrlkgj gjlg wglj wfgvkwuerhg ihjeorg arg rwgherger gelkrhge gegherkljg ergerjgnerg ergnerkjgner gergm,her,g kjfhw ewljfhwehf wefklwelf wef.wejflwkef we.gfjwerlfgw rgrlkgj gjlg wglj wfgvkwuerhg ihjeorg arg rwgherger gelkrhge gegherkljg ergerjgnerg ergnerkjgner gergm,her,g kjfhw ewljfhwehf wefklwelf wef.wejflwkef we.gfjwerlfgw rgrlkgj gjlg wglj wfgvkwuerhg ihjeorg arg rwgherger gelkrhge gegherkljg ergerjgnerg ergnerkjgner gergm,her,g kjfhw ewljfhwehf wefklwelf wef.wejflwkef we.gfjwerlfgw rgrlkgj gjlg wglj wfgvkwuerhg ihjeorg arg rwgherger gelkrhge gegherkljg ergerjgnerg ergnerkjgner gergm,her,g ', '2018-09-11', '10:30 AM', 0, '1536743824.png', '2018-09-05 09:07:09'),
(2, 'hngfcgahzfh', 'ghcfghqzjqfj hjgqfhg jyfgjyqv m iyjgjkq kuyj qjhgq jjg kjgigb jmhqgujx zqxbkucgwev fdkjgvhrkg ergrkgr grgkg er,mgerkgher,m gergkjerhgber ger,gherkg ergekghe,mgm rt,mgner,mg ergnerld gv', '2018-09-19', '06:00 AM', 0, '1536743811.jpg', '2018-09-05 09:35:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertisement_tbl`
--
ALTER TABLE `advertisement_tbl`
  ADD PRIMARY KEY (`AdvertisementId`);

--
-- Indexes for table `article_tbl`
--
ALTER TABLE `article_tbl`
  ADD PRIMARY KEY (`ArticleId`);

--
-- Indexes for table `login_tbl`
--
ALTER TABLE `login_tbl`
  ADD PRIMARY KEY (`UserId`);

--
-- Indexes for table `newsandevent_tbl`
--
ALTER TABLE `newsandevent_tbl`
  ADD PRIMARY KEY (`EventId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertisement_tbl`
--
ALTER TABLE `advertisement_tbl`
  MODIFY `AdvertisementId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `article_tbl`
--
ALTER TABLE `article_tbl`
  MODIFY `ArticleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `login_tbl`
--
ALTER TABLE `login_tbl`
  MODIFY `UserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `newsandevent_tbl`
--
ALTER TABLE `newsandevent_tbl`
  MODIFY `EventId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
