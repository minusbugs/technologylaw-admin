<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Adminlogin extends CI_Controller {
	public function __construct() {
		parent::__construct(); 
        $this->load->helper('url_helper');
        $this->load->library('session'); 
        $this->load->helper('form'); 
        $this->load->model('Login_model');
        $this->load->model('newsandevent_model');   
   }
   public function insertArticle(){
    $time=time();
    $articleTittle=$this->input->post('articlename');
    $articleDescription=$this->input->post('articledescription');
    $image=$_FILES['articleimage']['name'];
      $config['upload_path']          = './assets/newsandevent/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $images = explode('.',$image);
    $articleImage=$time.'.'.end($images);
    //$config['max_size']             = 2000;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;
    $config['file_name'] = $articleImage;
    $this->load->library('upload', $config);
    $this->upload->initialize($config); 
    if ( ! $this->upload->do_upload('articleimage'))
    {
      $data=array('ArticleTittle' =>$articleTittle,
      'ArticleDescription'=>$articleDescription);
      $result=$this->articles_model->addData($data);
      if ($result==1) {
        redirect('Adminlogin/addArticles/1');
      }else{
        echo "Data Not inseated";
      }
    }
    else{
      $data=array('ArticleTittle' =>$articleTittle,
      'ArticleDescription'=>$articleDescription,
      'ArticleImage'=>$articleImage);
      $result=$this->articles_model->addData($data);
      if ($result==1) {
        redirect('Adminlogin/addArticles/1');
      }else{
        redirect('Adminlogin/addArticles/2');
      }
    }
  }
 }