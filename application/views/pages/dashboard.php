<?php
  $this->load->view('includes/header');
  $this->load->view('includes/leftpanel');
?>
  <div class="mainpanel">

    <!--<div class="pageheader">
      <h2><i class="fa fa-home"></i> Dashboard</h2>
    </div>-->

    <div class="contentpanel">

      <div class="row">
        <div class="col-md-12 col-lg-12 dash-left">
          <div class="panel panel-announcement">
            <ul class="panel-options">
              <li><a><i class="fa fa-refresh"></i></a></li>
              <li><a class="panel-remove"><i class="fa fa-remove"></i></a></li>
            </ul>
            

          <div class="row panel-quick-page">
            <div class="col-xs-4 col-sm-5 col-md-4 page-user">
              <div class="panel">
                 <a href="<?=site_url()?>/adminlogin/getNewsandevents">
                <div class="panel-heading">
                  <h4 class="panel-title">News & Events</h4>
                </div>
                <div class="panel-body">
                  <div class="page-icon"><i class="fa fa-calendar"></i></div>
                </div>
                <div class="panel-body dashboardstyle">
                  <h2 class="panel-title"><?=$newsandeventcount?></h2>
                </div>
                </a>
              </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 page-products">
              <div class="panel">
                <a href="<?=site_url()?>/adminlogin/advertisement">
                <div class="panel-heading">
                  <h4 class="panel-title">Advertisement</h4>
                </div>
                <div class="panel-body">
                  <div class="page-icon"><i class="fa fa-buysellads"></i></div>
                </div>
                <div class="panel-body dashboardstyle">
                  <h2 class="panel-title"><?=$advertisement?></h2>
                </div>
                </a>
              </div>
            </div>
            <div class="col-xs-4 col-sm-3 col-md-2 page-events">
              <div class="panel">
                <a href="<?=site_url()?>/adminlogin/articles/0">
                  <div class="panel-heading">
                    <h4 class="panel-title">Articles</h4>
                  </div>
                  <div class="panel-body">
                    <div class="page-icon"><i class="fa fa-newspaper-o"></i></div>
                  </div>
                  <div class="panel-body dashboardstyle">
                  <h2 class="panel-title"><?=$article?></h2>
                </div>
                </a>
              </div>
            </div>
            <div class="col-xs-4 col-sm-3 col-md-2 page-messages">
              <div class="panel">
                <a href="<?=site_url()?>/adminlogin/changePassword">
                  <div class="panel-heading">
                    <h4 class="panel-title">Settings</h4>
                  </div>
                  <div class="panel-body">
                    <div class="page-icon"><i class="icon ion-gear-a"></i></div>
                  </div>
                  <div class="panel-body dashboardstyle">
                  <h2 class=""><br></h2>
                </div>
                </a>
              </div>
            </div>
          </div><!-- row -->

        </div><!-- col-md-9 -->
      </div><!-- row -->

    </div><!-- contentpanel -->

  </div><!-- mainpanel -->

</section>

<?php
  $this->load->view('includes/footer');
?>